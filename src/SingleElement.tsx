type SingleElementProps = {
  id: string,
  index: number,
  x: number,
  y: number,
  width: number,
  height: number,
  rotation: number,
  color: string,
  setError: (arg0:string) => void
}

const validateProps = (props:SingleElementProps) => {
  if( !props.id || !props.x || !props.y || !props.width || !props.height || !props.rotation || !props.color
      || props.width <= 0 || props.height <= 0
    ) return false
  return true
}

export const SingleElement  = (props:SingleElementProps) => {
  const valid = validateProps(props)
  const {id, index, x, y, width, height, rotation, color, setError} = props
  if(!valid) {
    setError('Recieved data was not correct, try anothr set of shapes!')
  }
  const sin = Math.sin(rotation*Math.PI/180)
  const cos = Math.cos(rotation*Math.PI/180)
  const bbWidth = Math.abs(cos*width) + Math.abs(sin*height)
  const bbHeight = Math.abs(cos*height) + Math.abs(sin*width)

  return(
      <g key={id+index} style={{transform:`translate(${x}px, ${y}px)`}}>
        <rect 
          className='rotated-rectangle' 
          fill={color}  
          width={width} 
          height={height} 
          style={{
            transform:`rotate(${rotation}deg) translate(${-width/2}px, ${-height/2}px)`,
            transformOrigin: `${0}px ${0}px`
          }}
        >
        </rect>
        <circle fill="#FFFFFF" cx={0} cy={0} r="4"></circle>
        <text x={5} y="5" fill="#FFFFFF">
          <tspan>{rotation}°</tspan>
        </text>
        <rect 
          fill="none" 
          strokeWidth="2" 
          strokeOpacity="0.4" 
          stroke="#FF0000"
          width={bbWidth} 
          height={bbHeight} 
          style={{transform:`translate(${-bbWidth/2}px, ${-bbHeight/2}px)`}}>
        </rect>
      </g>
  )
}