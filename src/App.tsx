import { useState, ChangeEvent } from 'react';
import { SingleElement } from './SingleElement';

import './App.css';

const App = () => {

  const [ projectId, setProjectId ] = useState<string>('')
  const [ projectData, setProjectData ] = useState<any>()
  const [ loading, setLoading ] = useState<boolean>(false)
  const [ error, setError ] = useState<string>('')

  const onChange = (e:ChangeEvent<HTMLInputElement>) => {
    setProjectId(e.target.value)
  }

  const getInitialProjectInfo = async () => {
    const initialProjectInfo = await fetch('https://recruitment01.vercel.app/api/init')

    try{
      if(initialProjectInfo.ok) return await initialProjectInfo.json()
      else {
        setError('Something went wrong')
        setLoading(false)
        return false
      }
    }
    catch{
      setError('Something went wrong')
      setLoading(false)
    }
  }

  const getProjectInfo = (id: string) => {
    return fetch(`https://recruitment01.vercel.app/api/project/${id}`)
  }

  const fetchData = async () => {
    setLoading(true)
    setError('')
    let id
    if(projectId === '') {
      const initialProjectInfo = await getInitialProjectInfo()
      id = await initialProjectInfo.id
    }else  id = projectId
    getProjectInfo(id)
    .then((response)=> {
      if(response.ok) return response.json()
      else {
        setError('Something went wrong')
        setLoading(false)
      }
    })
    .then((data) => {
      setProjectData(data) 
      setLoading(false)
    })
    .catch(() => {
      setError('Something went wrong')
      setLoading(false)
    })
  }

  return (
    <div className='container'>
      <div className = 'control'>
        <label>Project ID: </label>
        <input type='text' onChange={onChange} placeholder='For random leave empty' />
        <button onClick={() => fetchData()} disabled={loading}>Fetch</button>
        <hr/>
      </div>
      {
        !loading?(
          <div className='result-container'>
            {(error === '' && projectData)?
              <>
                <div className='header'>
                  <p>Name: <span>{projectData.project.name}</span></p>
                  <p>ID: <span>{projectData.id}</span></p>
                </div>
                <div className='canvas-container'>
                  <svg version="1.1" className='container-svg' xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" preserveAspectRatio="xMidYMid meet">
                    <rect fill="#d0d0d0" width="100%" height="100%"></rect>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox={`0 0 ${projectData.project.width} ${projectData.project.height}`} >
                      <rect fill="#efefef" width="100%" height="100%"></rect>
                      {projectData.project.items.map((item:any, index:number)=> 
                        <SingleElement 
                          id = {item.id}
                          index = {index}
                          x = {item.x}
                          y = {item.y}
                          width = {item.width}
                          height = {item.height}
                          rotation = {item.rotation}
                          color = {item.color}
                          setError = {setError}
                        /> 
                      )}
                    </svg>
                  </svg>
                </div>
              </>
            : <>
                {error&&
                  <>
                    <p>{error}</p>
                    <img src='https://i.ibb.co/dKSg0QR/error.jpg' alt='error' />
                  </>
                }
              </>
            }
          </div>
        ): <p>loading</p>
      }
    </div>
  );
}

export default App;
